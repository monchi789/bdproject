from django.http import HttpResponse
from django.shortcuts import render
from blog.models import Post, Specie


# Create your views here.

# Home page
def home(request):
    # Load all the post from db(10)
    posts = Post.objects.all()[:11]
    # print(posts)

    species = Specie.objects.all()

    data = {

        'posts': posts,
        'species': species,

    }

    return render(request, 'home.html', data)


# Post page
def post(request, url):
    # Load ulr from DB
    posts = Post.objects.get(url=url)
    species = Specie.objects.all()

    # print(post)
    return render(request, 'posts.html', {'post': posts, 'species': species})


# Specie page
def specie(request, url):
    species = Specie.objects.get(url=url)
    posts = Post.objects.filter(specie=species)

    return render(request, 'specie.html', {'specie': species, 'posts': posts})
