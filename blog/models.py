from django.db import models
from django.utils.html import format_html

# Import the text editor
from ckeditor.fields import RichTextField


# Create your models here.

# Models of class Specie
class Specie(models.Model):
    specieId = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)
    description = models.TextField()
    url = models.CharField(max_length=100)
    image = models.ImageField(upload_to='species/')
    add_date = models.DateTimeField(auto_now_add=True, null=True)

    # Config image_tag
    def image_tag(self):
        return format_html(
            '<img src="/media/{}" style="width:40px; height:40px; border-radius:50%;" />'.format(self.image))

    # Overwrite __str__ method
    def __str__(self):
        return self.title


# Models of class Post
class Post(models.Model):
    postId = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200)
    content = RichTextField(blank=True, null=True)
    url = models.CharField(max_length=100)
    specie = models.ForeignKey(Specie, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='post/')

    # Config image_tag
    def image_tag(self):
        return format_html(
            '<img src="/media/{}" style="width:40px; height:40px; border-radius:50%;" />'.format(self.image))

    # Overwrite __str__ method
    def __str__(self):
        return self.title
