from django.contrib import admin
from .models import Specie, Post


# Register your models here.

# Register and configuration Species models
class SpeciesAdmin(admin.ModelAdmin):
    list_display = ('image_tag', 'title', 'description', 'url', 'add_date',)
    search_fields = ('title',)


admin.site.register(Specie, SpeciesAdmin)


# Register and configuration Post models
class PostAdmin(admin.ModelAdmin):
    list_display = ('image_tag', 'title', 'url', 'specie',)
    search_fields = ('title',)
    list_filter = ('specie',)
    list_per_page = 50


admin.site.register(Post, PostAdmin)
